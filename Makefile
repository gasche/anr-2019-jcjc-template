LATEXMK=latexmk --pdf --bibtex
SOURCES=first-proposal.tex second-proposal.tex

all:
	$(LATEXMK) $(SOURCES)

clean:
	$(LATEXMK) -C $(SOURCES)
